import React from "react";
import "./index.css";
import "./App.css";
import Navigation from "./components/shared/Navigation";
import Footer from "./components/shared/Footer";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Home from "./components/pages/Home";
import Work from "./components/pages/Work";
import About from "./components/pages/About";
import Contact from "./components/pages/Contact";
import Login from "./components/pages/Login";
import Register from "./components/pages/Register";
import Messages from "./components/pages/Messages";
import PrivateRoute from "./components/shared/PrivateRoute";
import ScrollToTop from "./components/shared/ScrollToTop";

function App() {
  return (
    <BrowserRouter>
      <ScrollToTop />
      <Navigation />
      <Switch>
        <Redirect exact from="/" to="/home" />
        <Route path="/home">
          <Home />
        </Route>
        <Route exact path="/work" component={Work} />
        <Route exact path="/about" component={About} />
        <Route exact path="/contact" component={Contact} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        <PrivateRoute path="/messages">
          <Messages />
        </PrivateRoute>
      </Switch>
      <Footer />
    </BrowserRouter>
  );
}

export default App;
