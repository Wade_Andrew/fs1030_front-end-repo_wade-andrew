import React, { useState } from "react";
import "./Login.css";
import { Button, FormGroup, Label, Input, Card, CardBody, CardText } from "reactstrap";
import { Link as RouteLink } from "react-router-dom";
import { useHistory, useLocation, Link } from "react-router-dom";
import Swal from "sweetalert2";

const Login = () => {
  let history = useHistory();
  let location = useLocation();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [auth, setAuth] = useState(true);

  const loginSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch("http://localhost:4000/auth", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email, password }),
    });
    const payload = await response.json();
    if (response.status >= 400) {
      setAuth(false);
      Swal.fire({
        text: "Incorrect email or password. Please try again.",
        icon: "error",
        width: "30rem",
        padding: "0rem 0.1rem 1rem 0.1rem",
        confirmButtonColor: "#1b9cd8",
      });
    } else {
      sessionStorage.setItem("token", payload.token);

      let { from } = location.state || { from: { pathname: "/messages" } };
      history.replace(from);
    }
  };

  return (
    <main className="login-box">
      {/*{!auth && (
        <Card className="text-white bg-info text-center">
          <CardBody>
            <CardText className="text-white m-0">Incorrect email or password</CardText>
          </CardBody>  ** MAYBE ADD THIS LATER **
        </Card>
      )}*/}
      <form onSubmit={loginSubmit}>
        <p>Sign in to view your messages</p>
        <hr />
        <FormGroup>
          <Label for="emailEntry">Email</Label>
          <Input
            type="text"
            placeholder="Enter email address"
            name="email"
            id="emailEntry"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </FormGroup>

        <FormGroup>
          <Label for="passwordEntry">Password</Label>
          <Input
            type="password"
            placeholder="Enter password"
            name="password"
            id="passwordEntry"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </FormGroup>

        <Button color="success">Sign in</Button>
        <Link tag={RouteLink} className="p-0 register" to="/register">
          Register new user
        </Link>
      </form>
    </main>
  );
};

export default Login;
