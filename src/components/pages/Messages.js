import React, { useEffect, useState } from "react";
import "./Messages.css";
import { Container, Button } from "reactstrap";
import parseJwt from "../../helpers/authHelper";
import { useHistory } from "react-router-dom";
import { motion } from "framer-motion/dist/framer-motion";

const Messages = () => {
  let history = useHistory();
  const token = sessionStorage.getItem("token");
  const user = parseJwt(token).requestUser;

  const [messages, setMessages] = useState([]);
  const [animate, setAnimate] = useState(false);

  const handleDelete = (id) => {
    setMessages(messages.filter((entry) => entry.id !== id));
  };

  const logout = (event) => {
    event.preventDefault();
    sessionStorage.removeItem("token");
    history.push("/login");
  };
  useEffect(() => {
    const getData = async () => {
      const response = await fetch("http://localhost:4000/contact_form/entries", {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      setMessages(data);
    };
    getData();
  }, [token]);

  const animationVariant = {
    hidden: {
      x: "-100vw",
    },
    visible: {
      x: 0,
      transition: {
        delay: 0.5,
        when: "beforeChildren",
        staggerChildren: 0.5,
      },
    },
  };

  const listVariant = {
    hidden: {
      opacity: 0,
    },
    visible: {
      x: 0,
      opacity: 1,
    },
  };

  const menu = ["home", "work", "contact"];

  return (
    <Container fluid className="p-0">
      <aside className="listing-slug">
        <p>
          Welcome to your message page, <span className="green">{user}</span>.
        </p>
        <p>
          <em>
            Click email links to respond to messages. Click <strong>X</strong> to hide message.
          </em>
        </p>
        <Button onClick={logout} color="success">
          Logout
        </Button>
      </aside>
      <section>
        <table>
          <thead>
            <tr>
              <th style={{ width: "25px" }}></th>
              <th style={{ width: "20%" }}>Message ID #</th>
              <th style={{ width: "15%" }}>Name</th>
              <th style={{ width: "23%" }}>Email Address</th>
              <th style={{ width: "13%" }}>Phone Number</th>
              <th style={{ width: "29%" }}>Message</th>
            </tr>
          </thead>
          <tbody>
            {messages.length === 0 && (
              <tr>
                <td colSpan="6" className="text-center">
                  <strong>No messages found</strong>
                </td>
              </tr>
            )}
            {messages.length > 0 &&
              messages.map((entry) => (
                <tr>
                  <td key={entry.id}>
                    <button className="delete-button" onClick={() => handleDelete(entry.id)}>
                      X
                    </button>
                  </td>
                  <td>{entry.id}</td>
                  <td>{entry.name}</td>
                  <td>
                    <a href={`mailto:${entry.email}`} className="underline">
                      {entry.email}
                    </a>
                  </td>
                  <td>{entry.phoneNumber}</td>
                  <td>{entry.content}</td>
                </tr>
              ))}
          </tbody>
        </table>
        <div className="box-container">
          <motion.div
            className="box1"
            initial={{
              borderRadius: "5px",
            }}
            animate={{
              x: animate ? 50 : 0,
              y: animate ? -50 : 0,
              scale: animate ? 1.75 : 1,
              borderRadius: animate ? 0 : "5px",
            }}
            transition={{
              type: "spring",
              stiffness: 125,
            }}
            whileHover={{
              cursor: "pointer",
            }}
            onClick={() => setAnimate(!animate)}
          >
            <p>Line 1 information</p>
            <p>Line 2 information</p>
            <p>Line 3 information</p>
          </motion.div>
          <motion.div
            className="box2"
            drag
            dragConstraints={{
              top: 20,
            }}
            initial={{
              borderRadius: "10px",
            }}
            whileHover={{
              scale: 1.25,
              cursor: "pointer",
            }}
            whileTap={{
              scale: 1,
            }}
          >
            BOX 2
          </motion.div>
          <motion.div
            className="box3"
            variants={animationVariant}
            animate="visible"
            initial="hidden"
          >
            {menu.map((item) => {
              return (
                <motion.li className="boxItem" variants={listVariant}>
                  {item}
                </motion.li>
              );
            })}
          </motion.div>
          <motion.div
            className="box4"
            initial={{
              borderRadius: "10px",
            }}
          >
            BOX 4
          </motion.div>
        </div>
      </section>
    </Container>
  );
};

export default Messages;
