import React from "react";
import "./Work.css";
import { Container } from "reactstrap";
import portfolioData from "../../data/portfolioData";
import PortfolioItems from "../shared/PortfolioItems";
import SideNav from "../../components/shared/SideNav";

const Work = () => {
  const portfolio = portfolioData.map((item) => {
    return (
      <PortfolioItems
        key={item.id}
        image={item.image}
        alt={item.alt}
        descriptionTop={item.descriptionTop}
        descriptionBottom={item.descriptionBottom}
        link={item.link}
      />
    );
  });

  return (
    <Container fluid className="p-0">
      <aside className="work-animation">My Work</aside>
      {portfolio}
      <aside>
        <SideNav />
      </aside>
    </Container>
  );
};

export default Work;
