import React, { useState } from "react";
import "./Login.css";
import { Button, FormGroup, Label, Input } from "reactstrap";
import Swal from "sweetalert2";

const Register = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const formSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch("http://localhost:4000/users", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ name, email, password }),
    });
    const payload = await response.json();
    if (response.status >= 400) {
      Swal.fire({
        text: `Oops! ${payload.invalid.join(", ")}`,
        icon: "error",
        width: "30rem",
        padding: "0rem 0.1rem 1rem 0.1rem",
        confirmButtonColor: "#1b9cd8",
      });
    } else {
      Swal.fire({
        text: `Thanks for registering, ${name}. Click the button for the Login page`,
        icon: "success",
        iconColor: "#00be03",
        width: "30rem",
        padding: "0rem 0.1rem 1rem 0.1rem",
        confirmButtonText: '<a href="/login">Login Page</a>',
        confirmButtonColor: "#1a9c07",
      });
    }
    setName("");
    setEmail("");
    setPassword("");
  };

  return (
    <main className="login-box">
      <form onSubmit={formSubmit}>
        <p>Enter name, email and password to register</p>
        <hr />
        <FormGroup>
          <Label for="nameEntry">Name</Label>
          <Input
            type="text"
            placeholder="What's your name?"
            name="name"
            id="nameEntry"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />
        </FormGroup>
        <FormGroup>
          <Label for="emailEntry">Email</Label>
          <Input
            type="text"
            placeholder="Enter valid email"
            name="email"
            id="emailEntry"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </FormGroup>

        <FormGroup>
          <Label for="passwordEntry">Password</Label>
          <Input
            type="password"
            placeholder="Choose a secure password"
            name="password"
            id="passwordEntry"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </FormGroup>

        <Button color="info">Register User</Button>
      </form>
    </main>
  );
};

export default Register;
