import React from "react";
import "./About.css";
import { Container } from "reactstrap";
import employmentData from "../../data/employmentData";
import schoolData from "../../data/schoolData";
import EmploymentList from "../shared/EmploymentList";
import SchoolList from "../shared/SchoolList";
import SideNav from "../../components/shared/SideNav";

const About = () => {
  const workInfo = employmentData.map((item) => {
    return (
      <EmploymentList
        key={item.id}
        date={item.date}
        position={item.position}
        workplace={item.workplace}
        experience1={item.experience1}
        experience2={item.experience2}
        experience3={item.experience3}
      />
    );
  });

  const schoolInfo = schoolData.map((item) => {
    return (
      <SchoolList
        key={item.id}
        date={item.date}
        location={item.location}
        credential={item.credential}
      />
    );
  });

  return (
    <Container fluid className="p-0">
      <section>
        <p className="intro-box">
          <span className="hello">Hello!</span>Thank you for visiting my website. I'm a web
          developer living in Vancouver, BC. Creating CSS animations makes me{" "}
          <span className="happy">happy!</span> I'm a serious hiker, traveller and photographer. I
          would be happy to tell you about my adventures in the mountains of British Columbia or my
          many trips overseas.
          <br />
          <br />
          Take a look below for the boring details about my life. But really, I'd rather talk to you
          directly.{" "}
          <a href="/contact">
            Get in touch on the{" "}
            <strong>
              <span className="underline green">Contact</span>
            </strong>{" "}
            page.
          </a>
        </p>
      </section>
      <section className="about-box">
        <div>
          <h1>Work</h1>
          <br />
          <hr />
        </div>
        {workInfo}
      </section>
      <section className="about-box">
        <div>
          <h1>School</h1>
          <br />
          <hr />
        </div>
        {schoolInfo}
      </section>
      <aside>
        <SideNav />
      </aside>
    </Container>
  );
};

export default About;
