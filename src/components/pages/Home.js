import React from "react";
import "./Home.css";
import { Container } from "reactstrap";
import bioPic from "../../images/IMG_5023_2.jpg";
import SideNav from "../../components/shared/SideNav";

const Home = () => {
  return (
    <Container fluid className="p-0">
      <section>
        <div className="home-box">
          <div>
            <p className="wade">wade</p>
            <p className="andrew">
              andrew
              <span className="green-dash">_</span>
            </p>
          </div>
          <div>
            <p className="web">web</p>
            <p className="developer">developer</p>
          </div>
          <div className="pic-home">
            <img src={bioPic} alt="Wade Andrew" />
          </div>
        </div>
      </section>
      <aside>
        <SideNav />
      </aside>
    </Container>
  );
};

export default Home;
