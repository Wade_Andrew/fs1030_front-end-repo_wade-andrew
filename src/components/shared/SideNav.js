import React from "react";
import "./SideNav.css";

const SideNav = () => {
  return (
    <ul className="slug">
      <li className="vert-line-1" />
      <li>
        <a href="mailto:wadeandrew@icloud.com">
          <i className="bi bi-envelope-fill" />
        </a>
      </li>
      <li>
        <a href="#">
          <i className="bi bi-instagram" />
        </a>
      </li>
      <li>
        <a href="#">
          <i className="bi bi-linkedin" />
        </a>
      </li>
      <li className="vert-line-2" />
    </ul>
  );
};

export default SideNav;
