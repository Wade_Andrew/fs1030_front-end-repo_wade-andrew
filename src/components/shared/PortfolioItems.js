import React from "react";
import "../pages/Work.css";
import { Container } from "reactstrap";
import { useInView } from "react-intersection-observer";

const PortfolioItems = (props) => {
  const [ref, inView] = useInView({
    threshold: 0.4,
  });
  const [ref2, inView2] = useInView({
    threshold: 0.5,
  });

  return (
    <Container fluid className="p-0">
      <div className="work-box-flex">
        <div
          ref={ref}
          className={`work-flex-item-image ${inView ? "work-flex-item-image-fadein" : ""}`}
        >
          <img src={props.image} alt={props.alt} />
        </div>
        <div
          ref={ref2}
          className={`work-flex-item-description ${
            inView2 ? "work-flex-item-description-fade-down" : ""
          }`}
        >
          <p className="description-right">
            {props.descriptionTop}
            <br />
            <br />
            {props.descriptionBottom}
          </p>
          <p className="description-site-para">
            <a href={props.link} target="_blank" rel="noopener noreferrer">
              <span className="small-x-green">x</span>
              <span className="site-link">View Site</span>
            </a>{" "}
            - opens in a new tab
          </p>
        </div>
      </div>
    </Container>
  );
};

export default PortfolioItems;
