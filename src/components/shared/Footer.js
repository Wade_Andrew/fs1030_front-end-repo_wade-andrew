import React from "react";
import "./Footer.css";

const Footer = () => {
  return <footer>&copy; 2022 Wade Andrew</footer>;
};

export default Footer;
