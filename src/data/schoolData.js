const schoolData = [
  {
    id: 1,
    date: "2022 - 2023",
    location: "York University - Toronto, ON",
    credential: "Certificate in Full-Stack Web Development",
  },
  {
    id: 2,
    date: "1995 - 1997",
    location: "Southern Alberta Institute of Technology - Calgary, AB",
    credential: "Photography Diploma",
  },
  {
    id: 3,
    date: "1993 - 1994",
    location: "Red River College - Winnipeg, MB",
    credential: "Structural Drafting Diploma",
  },
  {
    id: 4,
    date: "1988 - 1992",
    location: "JH Bruns Collegiate - Winnipeg, MB",
    credential: "High School Diploma",
  },
];

export default schoolData;
